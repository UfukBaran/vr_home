﻿using UnityEngine;
using System.Collections;

public class ShowPanels : MonoBehaviour {

	public GameObject optionsPanelHome;	
	public GameObject optionsPanelClass;	
	public GameObject optionsPanelAmfi;					//Store a reference to the Game Object OptionsPanel 
	public GameObject optionsTint;			//Store a reference to the Game Object OptionsTint 
	public GameObject menuPanelHome;
	public GameObject menuPanelClass;
	public GameObject menuPanelAmfi;						//Store a reference to the Game Object MenuPanel 
	public GameObject pausePanel;							//Store a reference to the Game Object PausePanel 

	//Call this function to activate and display the Options panel during the main menu
	public void ShowOptionsPanelHome()
	{
		optionsPanelHome.SetActive(true);
		optionsTint.SetActive(true);
	}
	public void ShowOptionsPanelClass()
	{
		optionsPanelClass.SetActive(true);
		optionsTint.SetActive(true);
	}
	public void ShowOptionsPanelAmfi()
	{
		optionsPanelAmfi.SetActive(true);
		optionsTint.SetActive(true);
	}

	//Call this function to deactivate and hide the Options panel during the main menu
	public void HideOptionsPanelHome()
	{
		optionsPanelHome.SetActive(false);
		optionsTint.SetActive(false);
	}
	public void HideOptionsPanelClass()
	{
		optionsPanelClass.SetActive(false);
		optionsTint.SetActive(false);
	}
	public void HideOptionsPanelAmfi()
	{
		optionsPanelAmfi.SetActive(false);
		optionsTint.SetActive(false);
	}
	//Call this function to activate and display the main menu panel during the main menu
	public void ShowMenuHome()
	{
		menuPanelHome.SetActive (true);
	}
	public void ShowMenuClass()
	{
		menuPanelClass.SetActive (true);
	}
	public void ShowMenuAmfi()
	{
		menuPanelAmfi.SetActive (true);
	}
	//Call this function to deactivate and hide the main menu panel during the main menu
	public void HideMenuHome()
	{
		menuPanelHome.SetActive (false);
	}
	public void HideMenuClass()
	{
		menuPanelClass.SetActive (false);
	}
	public void HideMenuAmfi()
	{
		menuPanelAmfi.SetActive (false);
	}
	
	//Call this function to activate and display the Pause panel during game play
	public void ShowPausePanel()
	{
		pausePanel.SetActive (true);
		optionsTint.SetActive(true);
	}


	//Call this function to deactivate and hide the Pause panel during game play
	public void HidePausePanel()
	{
		pausePanel.SetActive (false);
		optionsTint.SetActive(false);

	}
}
